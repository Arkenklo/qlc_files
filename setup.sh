#!/bin/bash

# Delete fixture file if it's empty
rmdir ~/.qlcplus/fixtures/
# Link new fixtures. This will fail if the directory was not empty.
ln -s $(pwd)/fixtures/ ~/.qlcplus/fixtures
